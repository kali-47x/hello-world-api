<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     schema="Product",
 *     description="Produits",
 *     @OA\Property(type="integer", property="id"),
 *     @OA\Property(type="string", property="name"),
 *     @OA\Property(type="string", property="designation"),
 *     @OA\Property(type="integer", property="quantity"),
 *     @OA\Property(type="number", property="price"),
 *     @OA\Property(type="string", format="date-time", property="created_at"),
 *     @OA\Property(type="string", format="date-time", property="updated_at")
 * )
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'designation', 'price', 'quantity', 'created_at', 'updated_at'
    ];
}
