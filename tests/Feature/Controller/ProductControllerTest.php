<?php

namespace Tests\Feature\Controller;

use App\Models\Product;
use Database\Seeders\ProductSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    /**
     * Teste si les produits sont bien recupérés
     */
    public function testIndexProductSuccessfully()
    {
        for ($i = 1; $i <= 5; $i++) {
            Product::create($this->getRandomPayloads());
        }
        $this->json('GET', '/api/products')
            ->assertStatus(ResponseAlias::HTTP_OK)
            ->assertJsonStructure([
                '*' => ['name', 'designation', 'quantity', 'price', 'created_at', 'updated_at']
            ]);
    }

    /**
     * Teste si une entete 204 est renvoye s'il y'a aucun produit
     */
    public function testIndexProductEmpty() {
        $this->json('GET', '/api/products')
            ->assertStatus(ResponseAlias::HTTP_NO_CONTENT);
    }

    /**
     * Teste si un produit spécifique est bien recupéré
     */
    public function testShowProductSuccessfully()
    {
        $payload = $this->getRandomPayloads();
        $product = Product::create($payload);

        $this->json('GET', "/api/products/{$product->id}")
            ->assertStatus(200)
            ->assertExactJson([
                'id' => $product->id,
                'name' => $product->name,
                'designation' => $product->designation,
                'quantity' => $product->quantity,
                'price' => $product->price,
                'created_at' => $product->created_at,
                'updated_at' => $product->updated_at
            ]);
        $this->assertDatabaseHas(Product::class, $payload);
    }

    /**
     * Test si la récupération d'un produit innexistant échoue
     */
    public function testShowMissingProduct()
    {
        $this->json('GET', "/api/products/unk")
            ->assertForbidden();
    }

    /**
     * Teste si un produit se crée effectivement
     */
    public function testCreateProductSuccessfully()
    {
        $payload = $this->getRandomPayloads();
        $this->json('POST', '/api/products', $payload)
            ->assertStatus(ResponseAlias::HTTP_CREATED)
            ->assertJsonStructure([
                'name', 'designation', 'quantity', 'price', 'created_at', 'updated_at'
            ]);
        $this->assertDatabaseHas(Product::class, $payload);
    }

    /**
     * Teste si la création d'un produit avec des données incorrectes échoue
     */
    public function testCreateProductWithBadCredentials()
    {
        $payload = [
            'name' => 'Already Exist',
            'designation' => 'Already Exist',
            'quantity' => 'Already Exist',
            'price' => 'Already Exist'
        ];

        $this->json('POST', "/api/products", $payload)
            ->assertStatus(406)
            ->assertJsonStructure([
                'errors'
            ]);
        $this->assertDatabaseMissing(Product::class, $payload);
    }

    /*
     * Teste si un produit est bien supprimé
     */
    public function testDeleteProductSuccessfully()
    {
        $payload = $this->getRandomPayloads();
        $product = Product::create($payload);

        $this->json('DELETE', "/api/products/{$product->id}")
            ->assertOk();
        $this->assertDatabaseMissing(Product::class, $payload);
    }

    /**
     * Teste que la suppression d'un produit innexistant renvoie une erreur
     */
    public function testDeleteMissingProduct()
    {
        $this->json('DELETE', "/api/products/unk")
            ->assertForbidden();
    }

    /**
     * Teste qu'un produit est bien mis à jour
     */
    public function testUpdateProductSuccessfully()
    {
        $product = Product::create($this->getRandomPayloads());

        $payload = [
            'name' => 'Nouveau nom',
            'quantity' => 0
        ];

        $this->json('PATCH', "/api/products/{$product->id}", $payload)
            ->assertStatus(ResponseAlias::HTTP_OK)
            ->assertJsonFragment([
                'id' => $product->id,
                'name' => $payload['name'],
                'quantity' => $payload['quantity'],
                'price' => $product->price,
                'created_at' => $product->created_at,
            ]);
        $this->assertDatabaseHas(Product::class, $payload);
    }

    /**
     * Teste que la mis à jour d'un produit innexistant renvoi une erreur
     */
    public function testUpdateMissingProduct()
    {
        $this->json('PATCH', "/api/products/unk")
            ->assertForbidden();
    }

    /**
     * Teste que la mise à jour d'un produit existant avec des données incorrectes echou
     */
    public function testUpdateWithBadCredentials()
    {
        $product1 = Product::create($this->getRandomPayloads());
        $product2 = Product::create($this->getRandomPayloads());

        $this->json('PATCH', "/api/products/{$product2->id}", ['name' => $product1->name])
            ->assertStatus(406)
            ->assertJsonStructure([
                'errors'
            ]);
    }

    private function getRandomPayloads(): array
    {
        return [
            'name' => $this->faker->lastName,
            'designation' => $this->faker->text,
            'quantity' => $this->faker->numberBetween(0, 100),
            'price' => $this->faker->randomFloat(2, 50000, 500000)
        ];
    }
}
