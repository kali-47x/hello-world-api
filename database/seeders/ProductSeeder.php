<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 25; $i++) {
            DB::table('products')->insert([
                'name' => $faker->name,
                'designation' => $faker->text,
                'quantity' => $faker->numberBetween(0, 100),
                'price' => $faker->randomFloat(2, 50000, 500000)
            ]);
        }
    }
}
