## Bienvenue sur Jangoglo Hello World Api

Vous avez la une API qui permet de gerer les produits d'une boutique. Lorsque vous aurez cloné le projet, 
pensez a mettre a jour le fichier 'env' et a setup votre base de donnees.

## Tester l'Api

Vous trouvez l'ensemble des tests dans le sous repertoire test/Features/Controller

## Documentation de l'Api

Afin de mieux vous documenter, je vous invite a consulter le fichier swagger que vous trouverez dans le
sous repertoire public/swagger. A l'interieur vous aurez un fichier swagger.json qui decrit precisement 
le comportement de notre Api

## MERCI
