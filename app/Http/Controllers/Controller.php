<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * @OA\Info (title="Jangolo Hello World Api Description", version="1.0.0")
 * @OA\Server (
 *     url="http://localhost:8000/api",
 *     description="Jangolo Hello World Api Description"
 * )
 * @OA\Parameter(
 *      in="path",
 *      name="id",
 *      description="ID de la ressource",
 *      required=true,
 *      @OA\Schema(type="integer")
 * )
 * @OA\Response(
 *      response="NotFound",
 *      description="la ressource demandée n'a pas été trouvée"
 * )
 * @OA\Response(
 *      response="Forbidden",
 *      description="Impossible de traiter la ressource demandée"
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
