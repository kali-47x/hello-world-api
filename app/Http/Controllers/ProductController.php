<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ProductController extends Controller
{
    /**
     * @OA\Get(
     *     path="/products",
     *     summary="Recuperer tous les produits",
     *     @OA\Response(
     *          response="200",
     *          description="Tous les produits",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product"))
     *     ),
     *     @OA\Response(
     *          response="204",
     *          description="Aucun produit trouvé"
     *     )
     * )
     * @return Response
     */
    public function index(): Response
    {
        $products = Product::all();
        if ($products->count() !== 0) {
            return new Response($products, ResponseAlias::HTTP_OK);
        }
        return new Response([], ResponseAlias::HTTP_NO_CONTENT);
    }

    /**
     * @OA\Post(
     *     path="/products",
     *     summary="Ajouter un nouveau produit",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "designation", "quantity", "price"},
     *              @OA\Property(type="string", property="name"),
     *              @OA\Property(type="string", property="designation"),
     *              @OA\Property(type="integer", property="quantity"),
     *              @OA\Property(type="number", property="price"),
     *          )
     *     ),
     *     @OA\Response(
     *          response="201",
     *           description="Nouveau produit crée",
     *          @OA\JsonContent(ref="#/components/schemas/Product")
     *     ),
     *     @OA\Response (
     *          response="406",
     *          description="Données de l'utilisateur incorrectes"
     *     )
     * )
     * Create new product in database
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request): Response
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:products|max:50',
            'designation' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric|between:1,500000'
        ]);
        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()], ResponseAlias::HTTP_NOT_ACCEPTABLE);
        }
        $product = new Product($validator->validated());
        $product->save();
        return new Response($product, ResponseAlias::HTTP_CREATED);
    }

    /**
     * @OA\Get(
     *     path="/products/{id}",
     *     summary="Recuperer un produit spécifique",
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response="200",
     *          description="Informations sur le produit demandé",
     *          @OA\JsonContent(ref="#/components/schemas/Product")
     *     ),
     *     @OA\Response(response="403", ref="#/components/responses/Forbidden")
     * )
     * @param int|string $productId
     * @return Response
     */
    public function show($productId): Response
    {
        $product = Product::find($productId);
        if (!$product) {
            return new Response(['message' => "Impossible de traiter le produit demandé"], ResponseAlias::HTTP_FORBIDDEN);
        }

        return new Response($product, ResponseAlias::HTTP_OK);
    }

    /**
     * @OA\Patch (
     *     path="/products/{id}",
     *     summary="Mettre a jour les informations d'un produit",
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(
     *     required=true,
     *          @OA\JsonContent(
     *              @OA\Property(type="string", property="name"),
     *              @OA\Property(type="string", property="designation"),
     *              @OA\Property(type="integer", property="quantity"),
     *              @OA\Property(type="number", property="price"),
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *           description="Le produit a bien été mis à jour",
     *          @OA\JsonContent(ref="#/components/schemas/Product")
     *     ),
     *     @OA\Response(response="403", ref="#/components/responses/Forbidden"),
     *     @OA\Response (response="406", description="Données de l'utilisateur incorrectes")
     * )
     * Update product in database
     * @param Request $request
     * @param int|string $productId
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, $productId): Response
    {
        $product = Product::find($productId);
        if (!$product) {
            return new Response(['message' => "Impossible de traiter le produit demandé"], ResponseAlias::HTTP_FORBIDDEN);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'unique:products|max:50',
            'quantity' => 'numeric',
            'price' => 'numeric|between:1,500000'
        ]);

        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()], ResponseAlias::HTTP_NOT_ACCEPTABLE);
        }

        $product->update($validator->validated());
        return new Response($product, 200);
    }

    /**
     * @OA\Delete(
     *     path="/products/{id}",
     *     summary="Suppriner un produit spécifique",
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *          response="200",
     *          description="Produit Supprimé"
     *     ),
     *     @OA\Response(response="403", ref="#/components/responses/Forbidden")
     * )
     * @param int|string $productId
     * @return Response
     */
    public function destroy($productId): Response
    {
        $product = Product::find($productId);
        if (!$product) {
            return new Response(['message' => "Impossible de traiter le produit demandé"], ResponseAlias::HTTP_FORBIDDEN);
        }
        $product->delete();
        return new Response(['message' => "Le produit a bien été suprrimé."], ResponseAlias::HTTP_OK);
    }
}
